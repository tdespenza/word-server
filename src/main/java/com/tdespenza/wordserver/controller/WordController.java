package com.tdespenza.wordserver.controller;

import com.tdespenza.wordserver.config.WordConfig;
import com.tdespenza.wordserver.model.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~1/15/17.
 */
@Controller
@RefreshScope
public class WordController {

    @Autowired
    private WordConfig wordConfig;

    @GetMapping("/")
    @ResponseBody
    public Word getWord() {
        final String[] wordArray = wordConfig.getWords().split(",");
        final int index = (int) Math.round(Math.random() * (wordArray.length - 1));

        return new Word(wordArray[index]);
    }
}
